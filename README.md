# How to use these files

The python file 'code.py' performs the training of the clustering algorithm (kmeans). Besides, it computes similarity matrices between films.

In order to run it, please type 'python3 code.py', it will show a plot of the distortion in function of the number of clusters, it will also train our clustering algorithm and store it in a pickle file. And compute the similarity matrices and store them in pickle files as well.
Besides, it will store our encoder (a transformation that we apply to our data set).

Finally, the python 'recommendation.py' file contains classes of User and Movie, and enables to give recommendations. Feel free to choose whatever values (User demographic info + films and ratings) you want in the __main__ in order to have recommendations.
