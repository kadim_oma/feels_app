import pickle
import pandas as pd

kmeanModel = pickle.load(open("./models/kmeans.pkl", "rb"))
enc =  pickle.load(open("./models/encoder", "rb"))
sim = pickle.load(open("./models/sim", "rb"))
sim_glob = pickle.load(open("./models/sim_glob", "rb"))
movies = pd.read_csv('./data/movies.csv')


class Movie:
    __label = 1

    def __init__(self, movie, rating):
        self.movie = movie
        self.rating = rating
    def get_id(self):
        return movies[movies.title == self.movie].movieId.values[0]

    def get_similar_movie(self):
        if Movie.get_id(self) in sim[self.__label].index:
            print('yesssssss')
            similar_score = sim[self.__label][Movie.get_id(self)] * (self.rating - 2.5)
            similar_score = similar_score.sort_values(ascending=False)
        else:
            similar_score = sim_glob[Movie.get_id(self)] * (self.rating - 2.5)
            similar_score = similar_score.sort_values(ascending=False)
        return similar_score
    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self, label):
        self.__label = label

class User:
    def __init__(self, age, gender, occupation,zip_code, movie1, movie2, movie3):
        self.age = age
        self.gender = gender
        self.occupation = occupation
        self.zip_code = zip_code
        self.movie1 = movie1
        self.movie2 = movie2
        self.movie3 = movie3

    def give_cluster(self):
        user_stupid = [self.age, self.gender, self.occupation, self.zip_code]

        user_stupid_df = pd.concat([pd.DataFrame([user_stupid[:1]]),
                                    pd.DataFrame(enc.transform([user_stupid[1:]]).todense())], axis=1)
        label_stupid_user = kmeanModel.predict(user_stupid_df)[0]
        return label_stupid_user

    def sum_scores_movies(self):
        similar_movies_stupid = pd.DataFrame()
        for mov in [self.movie1, self.movie2, self.movie3]:
            mov.label = User.give_cluster(self)
            similar_movies_stupid = similar_movies_stupid.append(mov.get_similar_movie())
        stupid = similar_movies_stupid.sum().sort_values(ascending=False)
        return stupid

    def give_recommendations(self):


        movie_ids = [self.movie1.get_id(), self.movie2.get_id(), self.movie3.get_id() ]
        stupid = User.sum_scores_movies(self)
        for k in range(5):
            print('recommendation '+ str(k))
            print(movies[movies.movieId.isin(stupid[~stupid.index.isin(movie_ids)].nlargest(5).index)].title.values[k] +' : '
                  + movies[movies.movieId.isin(stupid[~stupid.index.isin(movie_ids)].nlargest(5).index)].genres.values[k])
            print("====" * 9)


        return str(movies[movies.movieId.isin(stupid[~stupid.index.isin(movie_ids)].nlargest(5).index)].title.values)


if __name__ == '__main__':
    User1 = User(30,	'M' , 	'writer'	,'85711',
                 Movie('Toy Story (1995)', 1),
                 Movie('American President, The (1995)', 5),
                 Movie('Pretty Woman (1990)', 3))
    User1.give_recommendations()
