import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import OneHotEncoder
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist

import pickle




ratings  =  pd.read_csv('./data/ratings.csv' )
movies = pd.read_csv('./data/movies.csv')
users = pd.read_csv('./data/user.csv', header=None, names=['userId', 'age', 'gender', 'occupation', 'zip_code' ])
users = users.astype({"zip_code": str})


enc =OneHotEncoder(handle_unknown='ignore')
users_encoded = enc.fit_transform(users[['gender', 'occupation', 'zip_code']])
column_name = enc.get_feature_names(['gender', 'occupation', 'zip_code'])
one_hot_encoded_frame =  pd.DataFrame(users_encoded.todense(), columns= column_name)

df_concat = pd.concat([users, one_hot_encoded_frame], axis=1)

del df_concat['userId']

del df_concat['gender']
del df_concat['occupation']
del df_concat['zip_code']


X = df_concat.values

kmeanModel = KMeans(n_clusters=5).fit(X)





def standardize(row):
  new_row = (row - row.mean()) / (row.max() - row.min())
  return(new_row)
glob = ratings
glob = pd.crosstab(ratings['userId'], ratings['movieId'], margins=False,values=ratings['rating'] ,
                 dropna=False, aggfunc='mean').reset_index().fillna(0)
glob = glob.set_index('userId')
glob = glob.apply(standardize)
sim_glob = cosine_similarity(glob.T)
sim_glob = pd.DataFrame(sim_glob , index = glob.columns, columns = glob.columns)

d = {}
sim = {}
df_concat['label'] = kmeanModel.predict(X)

for k in range(5):
  users_id = df_concat[df_concat['label'] == k].index + 1
  d[k] = ratings[ratings.index.isin(users_id)]
  d[k] = pd.crosstab(d[k]['userId'], d[k]['movieId'], margins=False,values=d[k]['rating'] ,
                 dropna=False, aggfunc='mean').reset_index().fillna(0)
  d[k] = d[k].set_index('userId')
  d[k] = d[k].apply(standardize)
  sim[k] = cosine_similarity(d[k].T)
  sim[k] = pd.DataFrame(sim[k] , index = d[k].columns, columns = d[k].columns)





if __name__ == '__main__':
    print("====" * 9)
    distortions = []
    inertias = []
    mapping1 = {}
    mapping2 = {}

    X = df_concat.loc[:, df_concat.columns != 'label'].values
    K = range(1, 40)

    for k in K:
        # Building and fitting the model
        kmeanModel1 = KMeans(n_clusters=k).fit(X)
        kmeanModel1.fit(X)

        distortions.append(sum(np.min(cdist(X, kmeanModel1.cluster_centers_,
                                            'euclidean'), axis=1)) / X.shape[0])
        inertias.append(kmeanModel1.inertia_)

        mapping1[k] = sum(np.min(cdist(X, kmeanModel1.cluster_centers_,
                                       'euclidean'), axis=1)) / X.shape[0]
        mapping2[k] = kmeanModel1.inertia_
        print("====" * 9)
        print('the inertia for ' + str(k) + ' cluster')
        print(mapping2[k])
        print("====" * 9)

    plt.plot(K, distortions, 'bx-')
    plt.xlabel('Values of K')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method using Distortion')
    plt.show()


    with open("./models/encoder", "wb") as f:
        pickle.dump(enc, f)

    with open("./models/kmeans.pkl", "wb") as f:
        pickle.dump(kmeanModel, f)

    with open("./models/sim", "wb") as f:
        pickle.dump(sim, f)

    with open("./models/sim_glob", "wb") as f:
        pickle.dump(sim_glob, f)


